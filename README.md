# vtkxml-to-vtkhdf Conversion Script

vtkxml-to-vtkhdf.py converts VTK XML image data and unstructured grid, serial and
parallel formats to the VTK HDF format.

See
[VTK File Formats](https://kitware.github.io/vtk-examples/site/VTKFileFormats/)
for more information about these formats.

We include a simple test script called `test.sh` that converts a file stored
using each of the supported input formats into VTK HDF and then it checks the
results. The script should not produce any difference with the stored baselines.



